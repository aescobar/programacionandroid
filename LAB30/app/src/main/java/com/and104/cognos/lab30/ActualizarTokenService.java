package com.and104.cognos.lab30;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by aescobar on 5/5/2016.
 */
public class ActualizarTokenService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, RegistroAppService.class);
        startService(intent);
    }
}
