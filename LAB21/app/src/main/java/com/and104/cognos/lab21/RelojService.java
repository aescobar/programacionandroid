package com.and104.cognos.lab21;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

import java.util.TimerTask;
import java.util.logging.LogRecord;

/**
 * Created by aescobar on 21/4/2016.
 */
public class RelojService extends Service {

    private Timer relojTimer = new Timer();
    private static final long INTERVALO = 10;
    private static MainActivity updater;
    private String relojString = "";
    private Handler handler;

    public static void setActivityEscucha(MainActivity _updater){
        updater = _updater;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                updater.actualizarReloj(relojString);
            }
        };

        iniciarReljo();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        pararReloj();
    }

    public void iniciarReljo(){
        relojTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                relojString = new SimpleDateFormat("dd/MM/yy hh:mm:ss").format(new Date());
                handler.sendEmptyMessage(0);
            }
        }, 0, INTERVALO);
    }


    public void pararReloj(){
        if(relojTimer != null)
            relojTimer.cancel();
    }
}
