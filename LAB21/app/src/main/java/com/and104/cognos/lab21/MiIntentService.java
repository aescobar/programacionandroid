package com.and104.cognos.lab21;

import android.app.Activity;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by aescobar on 21/4/2016.
 */
public class MiIntentService extends IntentService {

    private static String TAG = "MiIntentService";

    public MiIntentService() {
        super("MiIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Uri datosUri = intent.getData();
        String urlArchivo = intent.getStringExtra("URL_DESCARGA");
        String nombreArchivo = datosUri.getLastPathSegment();

        InputStream inputStream = null;
        OutputStream outputStream = null;
        HttpURLConnection connection = null;

        try {
            URL url = new URL(urlArchivo);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            inputStream = connection.getInputStream();
            outputStream = new FileOutputStream("/sdcard/"+nombreArchivo);

            byte data[] = new byte[2096];
            long total = 0;
            int count;
            while ((count = inputStream.read(data)) != -1) {
                total += count;
                outputStream.write(data, 0, count);
            }

            Notification notificacion = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle("Archivo Descargado")
                    .setContentText("El archivo "+ nombreArchivo + "Fue descargado exitosamente")
                    .setTicker("Notificacion")
                    .setAutoCancel(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .build();
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, notificacion);


        } catch (MalformedURLException e) {
            Log.e(TAG, "Error en la URL" + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "El archivo no puede ser encontrado" + e.getMessage());
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            }catch (IOException ex){
                Log.e(TAG, "Error al cerrar Streams");
            }

            if(connection != null)
                connection.disconnect();
        }
    }
}
