package com.and104.cognos.lab21;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etxUrlIntent;
    private TextView txvReloj;
    private MiServiceBinder miServiceBinder;
    private Messenger messenger = null;


    private ServiceConnection servConection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MiServiceBinder.MIBinder binder = (MiServiceBinder.MIBinder) service;
            miServiceBinder = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private ServiceConnection messConection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            messenger = new Messenger(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etxUrlIntent = (EditText) findViewById(R.id.etxUrl);
        txvReloj = (TextView) findViewById(R.id.txvReloj);

        RelojService.setActivityEscucha(this);
    }


    public void ejecutarIntentService(View view){
        Intent intent = new Intent(MainActivity.this, MiIntentService.class);
        String urlArchivo = etxUrlIntent.getText().toString();
        intent.putExtra("URL_DESCARGA", urlArchivo);
        intent.setData(Uri.parse(urlArchivo));
        startService(intent);

    }


    public void ejecutarServicioReloj(View view){
        Intent servicio = new Intent(MainActivity.this, RelojService.class);
        startService(servicio);
    }

    public void detenerServicioReloj(View view){
        Intent servicio = new Intent(MainActivity.this, RelojService.class);
        stopService(servicio);
    }

    public void actualizarReloj(String tiempo){
        txvReloj.setText(tiempo);
    }


    public void iniciarBinder(View view){
        Intent i = new Intent(MainActivity.this, MiServiceBinder.class);
        bindService(i, servConection, Context.BIND_AUTO_CREATE);
    }

    public void obtenerNumero(View view){
        if(miServiceBinder != null){
            String resultado = Integer.toString(miServiceBinder.obtenerNumero());
            Toast.makeText(MainActivity.this, "El resultado es: " + resultado, Toast.LENGTH_LONG).show();
        }
    }

    public void terminarBinder(View view){
        if(miServiceBinder != null){
            miServiceBinder.stopForeground(true);
            unbindService(servConection);
            miServiceBinder = null;

        }
    }


    public void iniciarMessage(View view){

        Intent i = new Intent(MainActivity.this, MiServiceMessage.class);
        bindService(i, messConection, Context.BIND_AUTO_CREATE);

    }

    public void saludarMessage(View view){
       if(messenger != null){
           Message msg = Message.obtain(null, 1, "Ariel");
           try {
               messenger.send(msg);
           } catch (RemoteException e) {
               Log.e("MainActivity", "Error al enviar mensaje " + e.getMessage());
           }
       }
    }

    public void terminarMessage(View view){
        if(messenger != null){
            unbindService(messConection);
            messenger = null;

        }
    }



}
