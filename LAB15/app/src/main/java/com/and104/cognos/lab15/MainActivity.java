package com.and104.cognos.lab15;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.and104.cognos.lab15.app.MisPreferencias;

public class MainActivity extends AppCompatActivity {

    private EditText etxUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etxUsuario = (EditText) findViewById(R.id.extUsuario);
        MisPreferencias pref = new MisPreferencias(MainActivity.this);
        if(!pref.priveraVez()){
            iniciarSegunaActivity();
        }
    }

    private void iniciarSegunaActivity() {
        Intent i = new Intent(MainActivity.this, SegundaActivity.class);
        startActivity(i);
        finish();
    }

    public void guardarUsuario(View view){
        String usuario = etxUsuario.getText().toString();
        MisPreferencias pref = new MisPreferencias(MainActivity.this);
        pref.setUsuario(usuario.trim());
        iniciarSegunaActivity();
    }

}
