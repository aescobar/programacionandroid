package com.and104.cognos.lab15.app;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by aescobar on 12/4/2016.
 */
public class MisPreferencias {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;
    private static final String PREF_NOMBRE = "LAB15";
    private static final String IS_PRIMERAVEZ = "IS_PRIMERAVEZ";
    private static final String USUARIO = "USUARIO";

    public MisPreferencias(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREF_NOMBRE, 0);
        editor = preferences.edit();
    }

    public boolean priveraVez() {
        return preferences.getBoolean(IS_PRIMERAVEZ, true);
    }

    public void hacerViejo(boolean b) {
        if (b) {
            editor.putBoolean(IS_PRIMERAVEZ, false);
            editor.commit();
        }
    }

    public String getUsuario() {
        return preferences.getString(USUARIO, "");
    }

    public void setUsuario(String usuario){
        editor.putString(USUARIO, usuario);
        editor.commit();
    }

    public void eliminarPreferencias(){
        editor.remove(USUARIO);
        editor.remove(IS_PRIMERAVEZ);
        editor.commit();
    }



}
