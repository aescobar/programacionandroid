package com.and104.cognos.lab15.app;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.util.Objects;

/**
 * Created by aescobar on 12/4/2016.
 */
public class AppController extends Application {
    private static AppController instance;
    private RequestQueue requestQueue;
    private final String TAG = "AppController";

    public static synchronized AppController getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public RequestQueue getRequestQueue(){
        if(requestQueue == null)
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        return requestQueue;
    }

    public <T> void addRequestQueue(Request<T> req, String tag){
        if(tag == null || tag.isEmpty())
            req.setTag(TAG);
        else
            req.setTag(tag);
        getRequestQueue().add(req);
    }

    public void cancelRequests(Objects tag){
        if(requestQueue != null)
            requestQueue.cancelAll(tag);
    }


}
