package com.and104.cognos.lab15;

import android.app.DownloadManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.and104.cognos.lab15.app.AppController;
import com.and104.cognos.lab15.app.ConnectionDetector;
import com.and104.cognos.lab15.app.MisPreferencias;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SegundaActivity extends AppCompatActivity {

    private final String TAG = "LAB15-app";
    private TextView txvResultado;
    private String archivo = "Servicio.json";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
        txvResultado = (TextView) findViewById(R.id.extUsuario);
        MisPreferencias pref = new MisPreferencias(SegundaActivity.this);
        if(pref.priveraVez()){
            Toast.makeText(SegundaActivity.this, "Hola: "+ pref.getUsuario(), Toast.LENGTH_LONG).show();
            pref.hacerViejo(true);
        }else{
            Toast.makeText(SegundaActivity.this, "Bienvenido otra vez: "+ pref.getUsuario(), Toast.LENGTH_LONG).show();
        }
        ConnectionDetector cd = new ConnectionDetector(SegundaActivity.this);
        if(cd.isConnected())
            llamarServicio();
        else
            leerArchivo();
    }

    private void llamarServicio(){
        String url = "http://jsonplaceholder.typicode.com/posts/4";
        txvResultado.setText("Cargando....");

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,"http://jsonplaceholder.typicode.com/posts/4", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                String texto = "";
                try {
                    texto = response.getString("title");
                } catch (JSONException e) {
                    Log.e("LAB15", "Error al consumir el servicio " + e.getMessage());
                    Toast.makeText(SegundaActivity.this, "Error al consumir el servicio", Toast.LENGTH_LONG).show();
                }
                escribirEnArchivo(texto);
                txvResultado.setText("Desde INTERNERT:" + texto);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LAB15", "Error al consumir el servicio " + error);
                Toast.makeText(SegundaActivity.this, "Error al consumir el servicio " + error, Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addRequestQueue(request, TAG);

    }

    private void escribirEnArchivo(String datos){
        try {

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(archivo, Context.MODE_PRIVATE));
            outputStreamWriter.write(datos);
            outputStreamWriter.close();
        } catch (Exception e) {
            Log.e("SegundaActivity",e.getMessage());
        }
    }

    private void leerArchivo(){
        String datos = "";
        try {
            InputStream inputStream = openFileInput(archivo);
            if(inputStream != null){
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();
                String respuesta = "";
                while((respuesta = bufferedReader.readLine())!= null){
                    stringBuilder.append(respuesta);
                }
                inputStream.close();
                datos = stringBuilder.toString();
            }
            txvResultado.setText("DESDE ARCHIVO:" + datos);
        } catch (Exception e) {
            Log.e("SegundaActivity", e.getMessage());
        }
    }


}
