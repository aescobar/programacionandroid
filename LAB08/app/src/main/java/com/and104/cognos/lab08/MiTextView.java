package com.and104.cognos.lab08;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by aescobar on 4/4/2016.
 */
public class MiTextView extends TextView {

    private Paint lineaPaint;

    public MiTextView(Context context) {
        super(context);
        init();
    }

    public MiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public void init(){
        lineaPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        lineaPaint.setColor(Color.BLUE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawLine(0,0, getMeasuredHeight(),0,lineaPaint);
        canvas.drawLine(0,getMeasuredHeight(), getMeasuredWidth(), getMeasuredHeight(), lineaPaint);
        canvas.save();
        super.onDraw(canvas);
        canvas.restore();
    }
}
