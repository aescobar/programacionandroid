package com.and104.cognos.lab08;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by aescobar on 1/4/2016.
 */
public class ViewPersonalizado extends LinearLayout {
    private EditText etxTexto1;
    private Button btnMostrar;
    private TextView txvResutado;

    private OnClickViewPersonalizado listener;

    public ViewPersonalizado(Context context) {
        super(context);
        init();
    }

    public ViewPersonalizado(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setMensaje(String mensaje){
        txvResutado.setText(mensaje);
    }

    private void init(){
        String service = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(service);
        layoutInflater.inflate(R.layout.view_personalizado, this, true);

        etxTexto1 = (EditText) findViewById(R.id.etxTexto1);
        btnMostrar = (Button) findViewById(R.id.btnMostrar);
        txvResutado = (TextView) findViewById(R.id.txvResultado);

        asignarEventos();


    }


    public void setOnClickPersonalizado(OnClickViewPersonalizado l){
        listener = l;
    }


    private void asignarEventos(){
        btnMostrar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.mostrarDato(etxTexto1.getText().toString());
            }
        });


    }

}
