package com.and104.cognos.lab08;

/**
 * Created by aescobar on 1/4/2016.
 */
public interface OnClickViewPersonalizado {
    void mostrarDato(String dato);

    String leerDato();
}
