<?php

require_once "Empleado.php";
require_once "AreaDB.php";

/**
 * Description of EmpleadoDB
 *
 * @author aescobar
 */
class EmpleadoDB {

    //put your code here
    protected $mysqli;

    const LOCALHOST = '127.0.0.1';
    const USER = 'root';
    const PASSWORD = '';
    const DATABASE = 'android_test';

    public function __construct() {
        try {
            //conexión a base de datos
            $this->mysqli = new mysqli(self::LOCALHOST, self::USER, self::PASSWORD, self::DATABASE);
        } catch (mysqli_sql_exception $e) {
            //Si no se puede realizar la conexión
            http_response_code(500);
            exit;
        }
    }

    public function getEmpleado($cod = 0) {
        $stmt = $this->mysqli->prepare("SELECT * FROM empleado WHERE cod_empleado=? ; ");
        $stmt->bind_param('s', $cod);
        $stmt->execute();
        $result = $stmt->get_result();
        $emp = null;
        while(($row =  mysqli_fetch_assoc($result))) {
            $emp = new Empleado();
            $emp->nombre = $row['nombre'];
            $emp->codEmpleado = $row['cod_empleado'];
            $area = new AreaDB();
            $emp->area = $area->getArea($row['cod_area']); 
        }
        $stmt->close();
        return $emp;
    }

    public function getEmpleados() {
        $result = $this->mysqli->query('SELECT * FROM empleado');
        $empleados = array();
        while (($row = mysqli_fetch_assoc($result))) {
            $emp = new Empleado();
            $emp->nombre = $row['nombre'];
            $emp->codEmpleado = $row['cod_empleado'];
            $area = new AreaDB();
            $emp->area = $area->getArea($row['cod_area']);
            $empleados[] = $emp;
        }
        $result->close();
        return $empleados;
    }

    public function insert($obj) {
        $nombre = $obj->nombre;
        $area = $obj->area;
        $codArea = $area->codArea;
        $stmt = $this->mysqli->prepare("INSERT INTO empleado(nombre,cod_area) VALUES (?,?); ");
        $stmt->bind_param('ss', $nombre, $codArea);
        $r = $stmt->execute();
        $fr = $stmt->insert_id;
        $stmt->close();
        return $fr;
    }

    public function delete($id = 0) {
        $stmt = $this->mysqli->prepare("DELETE FROM empleado WHERE cod_empleado = ? ; ");
        $stmt->bind_param('s', $id);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }

    public function update($obj) {
        $cod = $obj -> codEmpleado;
        $nombre = $obj->nombre;
        $area = $obj->area;
        $codArea = $area->codArea;
        $stmt = $this->mysqli->prepare("UPDATE empleado SET nombre=?, cod_area = ? WHERE cod_empleado = ? ; ");
        $stmt->bind_param('sss', $nombre, $codArea, $cod);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }

}
