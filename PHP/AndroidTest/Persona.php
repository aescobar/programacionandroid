<?php

require_once "PersonaDB.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Empleado
 *
 * @author aescobar
 */
class Persona {

    public $id;
    public $nombre;
    public $img;
    public $nfc;

    function guardarPersona() {
        $obj = json_decode(file_get_contents('php://input'));
        if (empty($obj)) {
            echo 'Error: el objeto no existe';
        } else {
            $persona = new PersonaDB();
            $obj->codEmpleado = $persona->insert($obj);
            echo $obj->codEmpleado;
        }
    }

    function getPersona($id) {

        $db = new PersonaDB();
        $response = $db->getPersona($id);
        return $response;
    }
    
    function getPersonaRest() {

        $db = new PersonaDB();
        
        if (isset($_GET['nfc'])) {
            $response = $db->getPersona2($_GET['nfc']);
            echo json_encode($response, JSON_PRETTY_PRINT);
        } else{
          echo 'error el parametro NFC no fue proveido';
        }
        
    }

    function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb");

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        return $output_file;
    }

}
