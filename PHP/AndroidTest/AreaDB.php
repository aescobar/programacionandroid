<?php

require_once "Area.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AreaDB
 *
 * @author aescobar
 */
class AreaDB {
    protected $mysqli;
    const LOCALHOST = '127.0.0.1';
    const USER = 'root';
    const PASSWORD = '';
    const DATABASE = 'android_test';
 
    public function __construct() {           
        try{
            $this->mysqli = new mysqli(self::LOCALHOST, self::USER, self::PASSWORD, self::DATABASE);
        }catch (mysqli_sql_exception $e){
            http_response_code(500);
            exit;
        }     
    } 
    
    public function getArea($cod=0){      
        $stmt = $this->mysqli->prepare("SELECT * FROM area WHERE cod_area=? ; ");
        $stmt->bind_param('s', $cod);
        $stmt->execute();
        $result = $stmt->get_result(); 
        $area = new Area();
        while(($row =  mysqli_fetch_assoc($result))) {
            $area->nombreArea = $row['nombre_area'];
            $area->codArea = $row['cod_area']; 
        }
        $stmt->close();
        return $area;              
    }
    
    public function getAreas(){      
        $result = $this->mysqli->query("SELECT * FROM area;");
        $areas = array();
        while (($row = mysqli_fetch_assoc($result))) {
            $area = new Area();
            $area->nombreArea = $row['nombre_area'];
            $area->codArea = $row['cod_area'];
            $areas[] = $area;
        }
        $result->close();
        return $areas;
    }
}
