<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once "Empleado.php";
require_once "Area.php";
require_once "Persona.php";

/**
 * Description of ObjectHelper
 *
 * @author aescobar
 */
class ObjectHelper {

    //put your code here
    public function operaciones() {
        header('Content-Type: application/JSON');
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'GET'://consulta
                if ($_GET['action'] == 'empleado') {
                    $emp = new Empleado();
                    $emp->getEmpleados();
                } else if ($_GET['action'] == 'area') {
                    $ar = new Area();
                    $ar->getAreas();
                } else if ($_GET['action'] == 'persona') {
                    $ar = new Persona();
                    $ar->getPersonaRest();
                }else{
                    echo 'ERROR: La solicitud no puede ser ejecutada --';
                }
                break;
            case 'POST'://inserta
                if ($_GET['action'] == 'empleado') {
                    $emp = new Empleado();
                    $emp->guardarEmpleado();
                } else if ($_GET['action'] == 'persona') {
                    $emp = new Persona();
                    $emp->guardarPersona(); 
                }else {
                    echo 'ERROR: La solicitud no puede ser ejecutada --';
                }
                break;
            case 'PUT'://actualiza
                if ($_GET['action'] == 'empleado') {
                    $emp = new Empleado();
                    $emp->updateEmpleado();
                } else {
                    echo 'ERROR: La solicitud no puede ser ejecutada --';
                }
                break;
            case 'DELETE'://elimina
                if ($_GET['action'] == 'empleado') {
                    $emp = new Empleado();
                    $emp->eliminarEmpleado();
                } else {
                    echo 'ERROR: La solicitud no puede ser ejecutada --';
                }
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }

}
