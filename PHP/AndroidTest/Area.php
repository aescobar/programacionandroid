<?php

require_once "AreaDB.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Area
 *
 * @author aescobar
 */
class Area {
    
    public $nombreArea;
    public $codArea;
    
    function getAreas() {

        $db = new AreaDB();
        if (isset($_GET['cod'])) {
            $response = $db->getArea($_GET['cod']);
            echo json_encode($response, JSON_PRETTY_PRINT);
        } else { 
            $response = $db->getAreas();
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }
    
}
