<?php

require_once "Persona.php";

/**
 * Description of EmpleadoDB
 *
 * @author aescobar
 */
class PersonaDB {

    //put your code here
    protected $mysqli;

    const LOCALHOST = '127.0.0.1';
    const USER = 'root';
    const PASSWORD = '';
    const DATABASE = 'android_test';

    public function __construct() {
        try {
            //conexión a base de datos
            $this->mysqli = new mysqli(self::LOCALHOST, self::USER, self::PASSWORD, self::DATABASE);
        } catch (mysqli_sql_exception $e) {
            //Si no se puede realizar la conexión
            http_response_code(500);
            exit;
        }
    }

    public function insert($obj) {
        $nombre = $obj->nombre;
        $img = $obj->img;
        $stmt = $this->mysqli->prepare("INSERT INTO persona(nombre,img) VALUES (?,?); ");
        $stmt->bind_param('ss', $nombre, $img);
        $r = $stmt->execute();
        $fr = $stmt->insert_id;
        $stmt->close();
        return $fr;
    }
    
    public function getPersona($cod = 0) {
        $stmt = $this->mysqli->prepare("SELECT * FROM persona WHERE nfc=? ; ");
        $stmt->bind_param('s', $cod);
        $stmt->execute();
        $result = $stmt->get_result();
        $emp = null;
        while(($row =  mysqli_fetch_assoc($result))) {
            $emp = new Persona();
            $emp->nombre = $row['nombre'];
            $emp->id = $row['id'];
            $emp->img = $row['img']; 
        }
        $stmt->close();
        return $emp;
    }

public function getPersona2($cod = 0) {
        $stmt = $this->mysqli->prepare("SELECT * FROM persona WHERE nfc=? ; ");
        $stmt->bind_param('s', $cod);
        $stmt->execute();
        $result = $stmt->get_result();
        $emp = null;
        while(($row =  mysqli_fetch_assoc($result))) {
            $emp = new Persona();
            $emp->nombre = $row['nombre'];
            $emp->id = $row['id'];
            $emp->nfc = $row['nfc']; 
        }
        $stmt->close();
        return $emp;
    }
}
