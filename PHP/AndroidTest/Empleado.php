<?php

require_once "EmpleadoDB.php";
require_once "AreaDB.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Empleado
 *
 * @author aescobar
 */
class Empleado {

    public $codEmpleado;
    public $nombre;
    public $area;

    function getEmpleados() {

        $db = new EmpleadoDB();
        if (isset($_GET['cod'])) {
            $response = $db->getEmpleado($_GET['cod']);
            echo json_encode($response, JSON_PRETTY_PRINT);
        } else { 
            $response = $db->getEmpleados();
            echo json_encode($response, JSON_PRETTY_PRINT);
        }
    }

    function guardarEmpleado() {
        $obj = json_decode(file_get_contents('php://input'));
        if (empty($obj)) {
            echo 'Error: el objeto no existe';
        } else {
            $empleado = new EmpleadoDB();
            $obj->codEmpleado = $empleado->insert($obj);
            echo json_encode($obj, JSON_PRETTY_PRINT);
        }
    }

    function eliminarEmpleado() {
        $db = new EmpleadoDB();
        if (isset($_GET['cod'])) {
            $empleado = new EmpleadoDB();
            $resp = $empleado->delete($_GET['cod']);
            echo $resp;
        } else { 
            echo 'Error: no se puede realizar';
        }

    }

    function updateEmpleado() {
        $obj = json_decode(file_get_contents('php://input'));
        if (empty($obj)) {
            echo 'Error: el objeto no existe';
        } else {
            $empleado = new EmpleadoDB();
            $resp = $empleado->update($obj);
            echo $resp;
        }
        
        /*if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == 'empleado') {
                $obj = json_decode(file_get_contents('php://input'));
                $objArr = (array) $obj;
                if (empty($objArr)) {
                    $this->response(422, "error", "Nothing to add. Check json");
                } else if (isset($obj->name)) {
                    $db = new PeopleDB();
                    $db->update($_GET['id'], $obj->name);
                    $this->response(200, "success", "Record updated");
                } else {
                    $this->response(422, "error", "The property is not defined");
                }
                exit;
            }
        }   */     
    }

}
