package com.and104.cognos.lab09;

/**
 * Created by aescobar on 5/4/2016.
 */
public interface OnLoginListener {
    void onLogin(String usuario, String password);
}
