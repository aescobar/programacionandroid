package com.and104.cognos.lab09;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class TercerFragment extends Fragment {

    private CuadradoView cdvwCuadrado3;

    public TercerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // inflater.inflate(R.layout.fragment_tercer, container, false);
        final View containerView = inflater.inflate(R.layout.fragment_tercer, container, false);
        cdvwCuadrado3 = (CuadradoView) containerView.findViewById(R.id.cdvwCuadrado3);
        cdvwCuadrado3.setColorCuadrado(Color.BLACK);
        cdvwCuadrado3.setColorLabel(Color.RED);
        cdvwCuadrado3.setStringLabel("HACER CLICK");
        cdvwCuadrado3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cdvwCuadrado3.isSwitchColor()) {
                    cdvwCuadrado3.setColorCuadrado(Color.BLUE);
                    cdvwCuadrado3.setColorLabel(Color.YELLOW);
                    cdvwCuadrado3.setStringLabel("OK!!!!!!");
                    cdvwCuadrado3.setSwitchColor(false);
                }else{
                    cdvwCuadrado3.setColorCuadrado(Color.BLACK);
                    cdvwCuadrado3.setColorLabel(Color.RED);
                    cdvwCuadrado3.setStringLabel("HACER CLICK");
                    cdvwCuadrado3.setSwitchColor(true);
                }
            }
        });
        return containerView;
    }

}
