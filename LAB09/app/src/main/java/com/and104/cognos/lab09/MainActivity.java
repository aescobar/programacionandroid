package com.and104.cognos.lab09;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private LoginView lvwLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvwLogin = (LoginView)findViewById(R.id.lvwLogin);

        lvwLogin.setOnLoginListener(new OnLoginListener() {
            @Override
            public void onLogin(String usuario, String password) {
                //Validamos el usuario y la contraseña
                if (usuario.equals("demo") && password.equals("demo")) {
                    lvwLogin.setMensaje("Login correcto!");
                    Intent i = new Intent(MainActivity.this, FragmentsActivity.class);
                    MainActivity.this.startActivity(i);
                } else
                    lvwLogin.setMensaje("Vuelva a intentarlo.");
            }
        });
    }
}
