package com.and104.cognos.lab09;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by aescobar on 5/4/2016.
 */
public class CuadradoView extends View {

    private int colorCuadrado;
    private int colorLabel;
    private String stringLabel;
    private boolean switchColor;
    private Paint paint;

    public CuadradoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ViewCuadrado,0,0);
        try {
            stringLabel = a.getString(R.styleable.ViewCuadrado_stringLabel);
            colorCuadrado = a.getInt(R.styleable.ViewCuadrado_colorCuadrado, 0);
            colorLabel = a.getInt(R.styleable.ViewCuadrado_colorLabel, 0);
            switchColor = a.getBoolean(R.styleable.ViewCuadrado_switchColor, true);
        }catch(Exception ex){
            Log.e("CuadradoView", ex.getMessage());
        }finally {
            a.recycle();
        }
    }

    public int getColorCuadrado() {
        return colorCuadrado;
    }

    public void setColorCuadrado(int colorCuadrado) {
        this.colorCuadrado = colorCuadrado;
        invalidate();
        requestLayout();
    }

    public int getColorLabel() {
        return colorLabel;
    }

    public void setColorLabel(int colorLabel) {
        this.colorLabel = colorLabel;
        invalidate();
        requestLayout();
    }

    public String getStringLabel() {
        return stringLabel;
    }

    public void setStringLabel(String stringLabel) {
        this.stringLabel = stringLabel;
        invalidate();
        requestLayout();
    }

    public boolean isSwitchColor() {
        return switchColor;
    }

    public void setSwitchColor(boolean switchColor) {
        this.switchColor = switchColor;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setColor(colorCuadrado);
        canvas.drawRect(0, 0, getMeasuredWidth(), getMeasuredHeight(), paint);
        paint.setColor(colorLabel);
        paint.setTextSize(50);
        canvas.drawText(stringLabel, this.getMeasuredWidth() /2, this.getMeasuredHeight() / 2, paint);

    }
}
