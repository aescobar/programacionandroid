package com.and104.cognos.lab09;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by aescobar on 5/4/2016.
 */
public class LoginView extends LinearLayout {
    EditText txtUsuario;
    EditText txtPassword;
    Button btnLogin;
    TextView lblMensaje;

    private OnLoginListener listener;


    public LoginView(Context context) {
        super(context);
        inicializar();
    }

    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inicializar();
    }

    private void inicializar() {
        //Utilizamos el layout 'control_login' como interfaz del control
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li =
                (LayoutInflater) getContext().getSystemService(infService);
        li.inflate(R.layout.login_layout, this, true);

        //Obtenemoslas referencias a los distintos control
        txtUsuario = (EditText) findViewById(R.id.TxtUsuario);
        txtPassword = (EditText) findViewById(R.id.TxtPassword);
        btnLogin = (Button) findViewById(R.id.BtnAceptar);
        lblMensaje = (TextView) findViewById(R.id.LblMensaje);

        //Asociamos los eventos necesarios
        asignarEventos();
    }

    public void setOnLoginListener(OnLoginListener l) {
        listener = l;
    }

    private void asignarEventos() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                listener.onLogin(txtUsuario.getText().toString(),
                        txtPassword.getText().toString());
            }
        });
    }

    public void setMensaje(String msg) {
        lblMensaje.setText(msg);
    }
}

