package com.and104.cognos.lab11;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText etxTexto;
    private ListView ltvwLista;
    private ImageButton btnAgregar;
    private ArrayList<String> lista;
    private ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etxTexto = (EditText) findViewById(R.id.etxTexto);
        ltvwLista = (ListView) findViewById(R.id.ltvwLista);
        btnAgregar = (ImageButton) findViewById(R.id.btnAgregar);
        lista = new ArrayList<>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        ltvwLista.setAdapter(adapter);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = etxTexto.getText().toString();
                if(!texto.isEmpty()) {
                    lista.add(0,texto);
                    adapter.notifyDataSetChanged();
                    etxTexto.setText("");
                }else{
                    Toast.makeText(MainActivity.this, R.string.no_caracteres, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
