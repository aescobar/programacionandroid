package com.and104.cognos.lab22;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Spinner spnArea;
    private ListView ltvwEmpleados;
    private EditText etxNombre;
    private EditText etxCod;

    private final static String TAG = "MainActivity-REST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spnArea = (Spinner) findViewById(R.id.spnAreas);
        ltvwEmpleados = (ListView) findViewById(R.id.ltvwEmpleados);
        etxCod = (EditText) findViewById(R.id.txtId);
        etxNombre = (EditText) findViewById(R.id.txtNombre);

        final ArrayList<Area> lstArea = new ArrayList<>();
        lstArea.add(new Area("Sistemas", 100));
        lstArea.add(new Area("RRHH", 101));

        ArrayAdapter<Area> adapter = new ArrayAdapter<Area>(MainActivity.this, android.R.layout.simple_spinner_item, lstArea);
        spnArea.setAdapter(adapter);

        listar();

    }

    public void listar(){
        Thread th;
        th = new Thread(new Runnable() {
            @Override
            public void run() {
                JSONArray jsonArray = null;

                HttpURLConnection httpURLConnection = null;
                BufferedReader bufferedReader;
                StringBuilder stringBuilder;
                String linea, jsonString = null;

                try {
                    URL url = new URL("http://10.0.2.2/AndroidTest/empleado");
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    bufferedReader = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
                    stringBuilder = new StringBuilder();

                    while((linea = bufferedReader.readLine()) != null){
                        stringBuilder.append(linea + "\n");
                    }
                    jsonString = stringBuilder.toString();

                } catch (MalformedURLException e) {
                    Log.e(TAG, "Error en la URL" + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "La url no existe" + e.getMessage());
                }

                try {
                    jsonArray = new JSONArray(jsonString);
                } catch (JSONException e) {
                    Log.e(TAG, "Error conversion JSON" + e.getMessage());
                }

                final ArrayList<Empleado> lstEmpleados = new ArrayList<>();
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject itemEmpleado = null;
                    JSONObject itemArea = null;
                    Area area = null;
                    Empleado empleado = null;
                    try {
                        itemEmpleado = jsonArray.getJSONObject(i);
                        itemArea = itemEmpleado.getJSONObject("area");
                        area = new Area(itemArea.getString("nombreArea"),itemArea.getInt("codArea"));
                        empleado = new Empleado(itemEmpleado.getInt("codEmpleado"), itemEmpleado.getString("nombre"), area);
                        lstEmpleados.add(empleado);
                    } catch (JSONException e) {
                        Log.e(TAG, "Error conversion JSON" + e.getMessage());
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayAdapter<Empleado> adapterEmp = new ArrayAdapter<Empleado>(MainActivity.this, android.R.layout.simple_list_item_1, lstEmpleados);
                        ltvwEmpleados.setAdapter(adapterEmp);
                    }
                });


            }
        });
        th.start();

    }

}
