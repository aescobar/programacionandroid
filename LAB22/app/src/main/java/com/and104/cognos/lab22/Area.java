package com.and104.cognos.lab22;

/**
 * Created by aescobar on 25/4/2016.
 */
public class Area {
    private String nombreArea;
    private int codArea;

    public Area(String nombreArea, int codArea) {
        this.nombreArea = nombreArea;
        this.codArea = codArea;
    }

    public String getNombreArea() {
        return nombreArea;
    }

    public void setNombreArea(String nombreArea) {
        this.nombreArea = nombreArea;
    }

    public int getCodArea() {
        return codArea;
    }

    public void setCodArea(int codArea) {
        this.codArea = codArea;
    }

    @Override
    public String toString() {
        //super.toString()
        return nombreArea;
    }
}
