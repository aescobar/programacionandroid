﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SoapAndroid
{
    /// <summary>
    /// Descripción breve de SoapAndroid
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class SoapAndroid : System.Web.Services.WebService
    {

        [WebMethod]
        public Empleado[] getEmpleados()
        {
            Empleado[] emps = new Empleado[2];
            emps[0] = new Empleado(1,"Ivan", new Area(100, "Sistemas"));
            emps[1] = new Empleado(2, "Javier", new Area(100, "Sistemas"));
            return emps;
        }

        [WebMethod]
        public String insertarEmpleado(Empleado emp)
        {
            if (emp == null) {
                return "Error: Objeto no recibido";
            }
            
            return "Empleado registrado nombre: " + emp.nombre + " area:" + emp.area.nombreArea;
        }
    }
}
