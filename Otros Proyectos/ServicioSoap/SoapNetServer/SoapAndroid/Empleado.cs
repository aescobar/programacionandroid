﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapAndroid
{
    [Serializable]
    public class Empleado
    {
        public String nombre {get; set;}
        public int codEmpleado {get; set;}
        public Area area {get; set;}

        public Empleado() { }

        public Empleado(int _codEmpleado, String _nombre, Area _area) {
            this.codEmpleado = _codEmpleado;
            this.nombre = _nombre;
            this.area = _area;
        }
    }
}