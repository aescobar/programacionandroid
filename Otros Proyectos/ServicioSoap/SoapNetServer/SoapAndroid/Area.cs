﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoapAndroid
{
    [Serializable]
    public class Area
    {
        public int codArea { get; set; }
        public String nombreArea {get; set;}

        public Area() { }

        public Area(int _codArea, String _nombreArea) {
            this.codArea = _codArea;
            this.nombreArea = _nombreArea;
        
        }
    }
}