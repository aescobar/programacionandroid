package com.and104.cognos.soap;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

/**
 * Created by aescobar on 25/4/2016.
 */
public class Area implements KvmSerializable {

    private int codArea;
    private String nombreArea;

    public Area(int codArea, String nombreArea) {
        this.codArea = codArea;
        this.nombreArea = nombreArea;
    }

    public int getCodArea() {
        return codArea;
    }

    public void setCodArea(int codArea) {
        this.codArea = codArea;
    }

    public String getNombreArea() {
        return nombreArea;
    }

    public void setNombreArea(String nombreArea) {
        this.nombreArea = nombreArea;
    }

    @Override
    public String toString() {
        return nombreArea;
    }

    @Override
    public Object getProperty(int i) {
        switch(i)
        {
            case 0:
                return this.codArea;
            case 1:
                return this.nombreArea;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 2;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch(i)
        {
            case 0:
                codArea = Integer.parseInt(o.toString());
                break;
            case 1:
                nombreArea = o.toString();
                break;

            default:
                break;
        }

    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch(i)
        {
            case 0:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "codArea";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "nombreArea";
                break;


            default:break;
        }
    }
}
