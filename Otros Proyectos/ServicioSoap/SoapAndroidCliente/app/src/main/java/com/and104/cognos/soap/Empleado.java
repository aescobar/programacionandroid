package com.and104.cognos.soap;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

/**
 * Created by aescobar on 25/4/2016.
 */
public class Empleado implements KvmSerializable {

    private int codEmpleado;
    private String nombre;
    private Area area;

    public Empleado() {
    }

    public Empleado(int codEmpleado, String nombre, Area area) {
        this.codEmpleado = codEmpleado;
        this.nombre = nombre;
        this.area = area;
    }

    public int getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(int codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return nombre + " - " + codEmpleado + " - " + area.getCodArea();
    }

    @Override
    public Object getProperty(int i) {
        switch(i)
        {
            case 0:
                return this.codEmpleado;
            case 1:
                return this.nombre;
            case 2:
                return this.area;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 3;
    }

    @Override
    public void setProperty(int i, Object o) {
        switch(i)
        {
            case 0:
                codEmpleado = Integer.parseInt(o.toString());
                break;
            case 1:
                nombre = o.toString();
                break;
            case 2:
                area = (Area) o;
                break;
            default:
                break;
        }
    }

    @Override
    public void getPropertyInfo(int i, Hashtable hashtable, PropertyInfo propertyInfo) {
        switch(i)
        {
            case 0:
                propertyInfo.type = PropertyInfo.INTEGER_CLASS;
                propertyInfo.name = "codEmpleado";
                break;
            case 1:
                propertyInfo.type = PropertyInfo.STRING_CLASS;
                propertyInfo.name = "nombre";
                break;
            case 2:
                propertyInfo.type = Area.class;
                propertyInfo.name = "area";
                break;

            default:break;
        }
    }
}
