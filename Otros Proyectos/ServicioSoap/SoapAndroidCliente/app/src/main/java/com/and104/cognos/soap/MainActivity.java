package com.and104.cognos.soap;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.EmptyStackException;

public class MainActivity extends AppCompatActivity {

    private Spinner spnArea;
    private ListView ltvwEmpleados;
    private EditText etxNombre;
    private EditText etxId;
    private final static String TAG = "MainActivity-Soap";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spnArea = (Spinner) findViewById(R.id.spnAreas);
        ltvwEmpleados = (ListView) findViewById(R.id.ltvwEmpleados);
        etxId = (EditText) findViewById(R.id.txtId);
        etxNombre = (EditText) findViewById(R.id.txtNombre);

        final ArrayList<Area> lstArea = new ArrayList<>();
        lstArea.add(new Area(100, "Sistemas"));
        lstArea.add(new Area(101, "RRHH"));

        ArrayAdapter<Area> adapter = new ArrayAdapter<Area>(MainActivity.this,
                android.R.layout.simple_spinner_item, lstArea);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnArea.setAdapter(adapter);

        ltvwEmpleados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Empleado emp = (Empleado) ltvwEmpleados.getAdapter().getItem(position);
                etxId.setText(emp.getCodEmpleado() + "");
                etxNombre.setText(emp.getNombre());
                for (int i = 0; i < lstArea.size(); i++) {
                    if (lstArea.get(i).getCodArea() == emp.getArea().getCodArea()) {
                        spnArea.setSelection(i);
                        break;
                    }
                }
            }
        });

        TareaWSConsulta tw = new TareaWSConsulta();
        tw.execute();
    }

    public void actualizar(View view) {
        System.out.println("Actualizar.....");
        TareaWSConsulta tw = new TareaWSConsulta();
        tw.execute();

    }

    public void inserta(View view) {
        System.out.println("Actualizar.....");
        Empleado nuevoEmpleado = new Empleado(1, etxNombre.getText().toString(), (Area)spnArea.getSelectedItem());
        TareaWSInserta tw = new TareaWSInserta();
        tw.execute(nuevoEmpleado);

    }

    private class TareaWSConsulta extends AsyncTask<String, Void, ArrayList<Empleado>> {

        @Override
        protected ArrayList<Empleado> doInBackground(String... params) {
            ArrayList<Empleado> lstEmpleados = null;
            boolean resul = true;
            System.out.println("Iniciado.....");
            final String NAMESPACE = "http://tempuri.org/";
            final String URL = "http://10.0.2.2:22377/SoapAndroid.asmx";
            final String METHOD_NAME = "getEmpleados";
            final String SOAP_ACTION = "http://tempuri.org/getEmpleados";

            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            SoapSerializationEnvelope envelope =
                    new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transporte = new HttpTransportSE(URL);

            try {
                transporte.call(SOAP_ACTION, envelope);

                SoapObject resSoap = (SoapObject) envelope.getResponse();

                System.out.println("........" + resSoap);
                lstEmpleados = new ArrayList<>();
                for (int i = 0; i < resSoap.getPropertyCount(); i++) {
                    SoapObject ic = (SoapObject) resSoap.getProperty(i);

                    Empleado empleado = new Empleado();
                    System.out.println(ic.getProperty(0).toString());
                    empleado.setCodEmpleado(Integer.parseInt(ic.getProperty(1).toString()));
                    empleado.setNombre(ic.getProperty(0).toString());
                    SoapObject areaSo = (SoapObject) ic.getProperty(2);
                    Area ar = new Area(Integer.parseInt(areaSo.getProperty(0).toString()), areaSo.getProperty(1).toString());
                    empleado.setArea(ar);
                    lstEmpleados.add(empleado);
                }
            } catch (Exception e) {
                Log.e(TAG, "Error al consumir servicio" + e.getMessage());
            }

            return lstEmpleados;
        }

        protected void onPostExecute(ArrayList<Empleado> result) {

            if (result != null) {
                ArrayAdapter<Empleado> adaptador =
                        new ArrayAdapter<Empleado>(MainActivity.this,
                                android.R.layout.simple_list_item_1, result);

                ltvwEmpleados.setAdapter(adaptador);
            } else {
                Toast.makeText(MainActivity.this, "Error, no se pudo consumir el servicio", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private class TareaWSInserta extends AsyncTask<Empleado, Integer, String> {


        protected String doInBackground(Empleado... params) {
            String resp = null;
            System.out.println("Iniciado.....");
            final String NAMESPACE = "http://tempuri.org/";
            final String URL = "http://10.0.2.2:22377/SoapAndroid.asmx";
            final String METHOD_NAME = "insertarEmpleado";
            final String SOAP_ACTION = "http://tempuri.org/insertarEmpleado";
            PropertyInfo ppp = new PropertyInfo();
            ppp.setName("emp");
            ppp.setValue(params[0]);
            ppp.setType(Empleado.class);
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
            request.addProperty(ppp);
            SoapSerializationEnvelope envelope =
                    new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            envelope.addMapping(NAMESPACE, "Empleado", Empleado.class);
            envelope.addMapping(NAMESPACE, "Area", Area.class);

            HttpTransportSE transporte = new HttpTransportSE(URL);

            try {
                transporte.call(SOAP_ACTION, envelope);
                SoapPrimitive resSoap = (SoapPrimitive) envelope.getResponse();
                resp = resSoap.toString();
            } catch (Exception e) {
                Log.e(TAG, "Error al consumir servicio" + e.getMessage());
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
        }
    }
}
