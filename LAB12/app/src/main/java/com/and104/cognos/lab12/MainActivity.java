package com.and104.cognos.lab12;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickBtnClick(View view){
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setTitle("TEXTO TIT");
        dialog.setContentView(R.layout.mi_alerta);
        TextView txvContenido = (TextView) dialog.findViewById(R.id.txvMensajeAlerta);
        Button btnOk = (Button) dialog.findViewById(R.id.btnAlertaOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(
                        MainActivity.this, "OK",
                        Toast.LENGTH_SHORT)
                        .show();

                dialog.cancel();
            }
        });
        txvContenido.setText("MI TEXTOoooooooooo");

        dialog.show();

    }

    public void clickBtnClick2(View view){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        final String[] listaStrings = new String[12];
        listaStrings[0] = "Op1";
        listaStrings[1] = "Op2";
        listaStrings[2] = "Op3";

        alertDialog.setTitle("Seleccion opcion")
                .setItems(listaStrings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(
                                MainActivity.this,
                                "Seleccionaste:" + listaStrings[which],
                                Toast.LENGTH_SHORT)
                                .show();

                    }
                });

        alertDialog.create();
        alertDialog.show();

    }

    public void btnClickBtn3(View view){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        final CharSequence[] items = new CharSequence[3];

        items[0] = "Soltero/a";
        items[1] = "Casado/a";
        items[2] = "Divorciado/a";

        builder.setTitle("Estado Civil")
                .setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(
                                MainActivity.this,
                                "Seleccionaste: " + items[which],
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });


        builder.create();
        builder.show();
    }

    public void btnClickBtn4(View view){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("ESTO");
        alertDialog.setMessage("sdadsa");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
       /* MenuItem menuItem1 = menu.add(Menu.NONE,Menu.FIRST,Menu.NONE,"Prop");
        MenuItem menuItem2 = menu.add(Menu.NONE,Menu.FIRST,Menu.NONE,"Exit");*/
        MenuInflater mf = getMenuInflater();
        mf.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        Toast.makeText(
                MainActivity.this,
                "Seleccionaste: " + item.getItemId(),
                Toast.LENGTH_SHORT)
                .show();
        switch (item.getItemId()) {
            case (Menu.FIRST):
                //Log.v("ESTO"," ------"+item.getItemId());
                Toast.makeText(
                        MainActivity.this,
                        "Seleccionaste: " + item.getItemId(),
                        Toast.LENGTH_SHORT)
                        .show();
                /*Intent i = new Intent(this, Main2Activity.class);
                startActivity(i);*/
                return true;
        }
        return true;

    }
}
