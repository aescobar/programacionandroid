package bo.miteleferico.movilteleferico.front.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import bo.miteleferico.movilteleferico.R;
import bo.miteleferico.movilteleferico.back.common.Parametros;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventosFragment extends Fragment {

    private String titulo;
    private int pagina;


    public EventosFragment() {
        // Required empty public constructor
    }


    public static EventosFragment newInstance(int pagina, String titulo) {
        EventosFragment eventosFragment = new EventosFragment();
        Bundle args = new Bundle();
        args.putInt(Parametros.PARAM_PAGINA, pagina);
        args.putString(Parametros.PARAM_TITULO, titulo);
        eventosFragment.setArguments(args);
        return eventosFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_eventos, container, false);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pagina = getArguments().getInt(Parametros.PARAM_PAGINA, 0);
        titulo = getArguments().getString(Parametros.PARAM_TITULO);
    }
}
