package bo.miteleferico.movilteleferico.front.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bo.miteleferico.movilteleferico.R;
import bo.miteleferico.movilteleferico.back.common.Parametros;


/**
 * A simple {@link Fragment} subclass.
 */
public class PromocionesFragment extends Fragment {

    private String titulo;
    private int pagina;

    public PromocionesFragment() {
        // Required empty public constructor
    }


    public static PromocionesFragment newInstance(int pagina, String titulo) {
        PromocionesFragment noticiasFragment = new PromocionesFragment();
        Bundle args = new Bundle();
        args.putInt(Parametros.PARAM_PAGINA, pagina);
        args.putString(Parametros.PARAM_TITULO, titulo);
        noticiasFragment.setArguments(args);
        return noticiasFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_promociones, container, false);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pagina = getArguments().getInt(Parametros.PARAM_PAGINA, 0);
        titulo = getArguments().getString(Parametros.PARAM_TITULO);

    }
}
