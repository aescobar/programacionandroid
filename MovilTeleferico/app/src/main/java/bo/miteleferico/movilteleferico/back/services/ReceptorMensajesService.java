package bo.miteleferico.movilteleferico.back.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import bo.miteleferico.movilteleferico.R;
import bo.miteleferico.movilteleferico.back.common.Parametros;
import bo.miteleferico.movilteleferico.front.activitys.MainActivity;

/**
 * Created by ichacolla on 05/05/2016.
 */
public class ReceptorMensajesService extends GcmListenerService {
    private final static String TAG = "ReceptorMensajesService";
    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        String mensaje = data.getString(Parametros.MSG_PARAM_MENSAJE);
        String titulo = data.getString(Parametros.MSG_PARAM_TITULO);
        String ticker = data.getString(Parametros.MSG_PARAM_TICKER);
        Log.i(TAG, "mensaje: " + mensaje+" titulo: "+ titulo);
        enviarNotificacion(mensaje,titulo,ticker);
    }

    private void enviarNotificacion(String mensaje, String titulo,String ticker) {
        Intent intent = new Intent(this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);
        //sonido
        Uri sonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificacion = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.icon)
                .setContentTitle(titulo).setContentText(mensaje).setAutoCancel(true).setContentIntent(pendingIntent)
                .setSound(sonido)
                .setTicker(ticker)
                .setVibrate(new long[] {100, 250, 100, 500});
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,notificacion.build());

    }
}
