package bo.miteleferico.movilteleferico.back.common;

/**
 * Created by ichacolla on 05/05/2016.
 */
public class Parametros {

    public final static String TOKEN_REGISTRO = "TOKEN REGISTRO";
    public final static String RECEIVER_REGISTRO_COMPLETO="RECEIVER_REGISTRO_COMPLETO";
    public final static String MSG_PARAM_MENSAJE="message";
    public final static String MSG_PARAM_TITULO="title";
    public final static String MSG_PARAM_TICKER="tickerText";


    public final static String PARAM_PAGINA = "pagina";
    public final static String PARAM_TITULO = "titulo";

}
