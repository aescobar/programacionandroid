package bo.miteleferico.movilteleferico.front.fragments;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import bo.miteleferico.movilteleferico.R;
import bo.miteleferico.movilteleferico.back.common.Parametros;
import bo.miteleferico.movilteleferico.back.database.FeedDatabase;
import bo.miteleferico.movilteleferico.back.database.ScriptDatabase;
import bo.miteleferico.movilteleferico.back.model.rss.Rss;
import bo.miteleferico.movilteleferico.back.web.VolleySingleton;
import bo.miteleferico.movilteleferico.back.web.XmlRequest;
import bo.miteleferico.movilteleferico.front.activitys.NoticiaDetalleActivity;
import bo.miteleferico.movilteleferico.front.adapters.FeedAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoticiasFragment extends Fragment {

    /*
    Etiqueta de depuración
     */
    private static final String TAG = NoticiasFragment.class.getSimpleName();

    /*
    URL del feed
     */
    public static final String URL_FEED = "http://www.forbes.com/most-popular/feed/";

    /*
    Lista
     */
    private ListView listView;

    /*
    Adaptador
     */
    private FeedAdapter adapter;



    private String titulo;
    private int pagina;

    public static NoticiasFragment newInstance(int pagina, String titulo) {
        NoticiasFragment noticiasFragment = new NoticiasFragment();
        Bundle args = new Bundle();
        args.putInt(Parametros.PARAM_PAGINA, pagina);
        args.putString(Parametros.PARAM_TITULO, titulo);
        noticiasFragment.setArguments(args);
        return noticiasFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pagina = getArguments().getInt(Parametros.PARAM_PAGINA, 0);
        titulo = getArguments().getString(Parametros.PARAM_TITULO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewContainer = inflater.inflate(R.layout.fragment_noticias, container, false);


        listView = (ListView)viewContainer.findViewById(R.id.ltvNoticias);

        ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            VolleySingleton.getInstance(getContext()).addToRequestQueue(
                    new XmlRequest<>(
                            URL_FEED,
                            Rss.class,
                            null,
                            new Response.Listener<Rss>() {
                                @Override
                                public void onResponse(Rss response) {
                                    // Caching
                                    FeedDatabase.getInstance(getContext()).
                                            sincronizarEntradas(response.getChannel().getItems());
                                    // Carga inicial de datos...
                                    new LoadData().execute();
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d(TAG, "Error Volley: " + error.getMessage());
                                }
                            }
                    )
            );
        } else {
            Log.i(TAG, "La conexión a internet no está disponible");
            adapter= new FeedAdapter(
                    getActivity(),
                    FeedDatabase.getInstance(getActivity()).obtenerEntradas(),
                    SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
            listView.setAdapter(adapter);
        }




        // Regisgrar escucha de la lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor c = (Cursor) adapter.getItem(position);

                // Obtene url de la entrada seleccionada
                String url = c.getString(c.getColumnIndex(ScriptDatabase.ColumnEntradas.URL));

                // Nuevo intent explícito
                Intent i = new Intent(getContext(), NoticiaDetalleActivity.class);

                // Setear url
                i.putExtra("url-extra", url);

                // Iniciar actividad
                startActivity(i);
            }
        });



        return viewContainer;
    }

    public class LoadData extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... params) {
            // Carga inicial de registros
            return FeedDatabase.getInstance(getActivity()).obtenerEntradas();

        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);

            // Crear el adaptador
            adapter = new FeedAdapter(
                    getActivity(),
                    cursor,
                    SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

            // Relacionar la lista con el adaptador
            listView.setAdapter(adapter);
        }
    }

}
