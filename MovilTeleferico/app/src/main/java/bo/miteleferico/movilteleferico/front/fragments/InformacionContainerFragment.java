package bo.miteleferico.movilteleferico.front.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

import bo.miteleferico.movilteleferico.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformacionContainerFragment extends Fragment {

    FragmentPagerAdapter adapterViewPager;

    public final static int ITEM_NOTICIAS = 0;
    public final static int ITEM_PROMOCIONES = 1;
    public final static int ITEM_EVENTOS = 2;

    public InformacionContainerFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewContainer = inflater.inflate(R.layout.fragment_informacion_container, container, false);
        ViewPager vpPager = (ViewPager) viewContainer.findViewById(R.id.vpPager);
        adapterViewPager = new PaginaAdapter(getFragmentManager());
        vpPager.setAdapter(adapterViewPager);

        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) viewContainer.findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(vpPager);

        return viewContainer;
    }

     public String parseString(int id){
         return getString(id);
     }


    private class PaginaAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 3;
        public final String[] titulos = {getString(R.string.gral_noticias),
                getString(R.string.gral_promociones),
                getString(R.string.gral_eventos)};

        public PaginaAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case ITEM_NOTICIAS:
                    return NoticiasFragment.newInstance(ITEM_NOTICIAS, getString(R.string.gral_noticias));
                case ITEM_PROMOCIONES:
                    return PromocionesFragment.newInstance(ITEM_PROMOCIONES, getString(R.string.gral_promociones));
                case ITEM_EVENTOS:
                    return EventosFragment.newInstance(ITEM_EVENTOS, getString(R.string.gral_eventos));

                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titulos[position];
        }

    }
}
