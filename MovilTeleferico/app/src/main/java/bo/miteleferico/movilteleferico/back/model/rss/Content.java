package bo.miteleferico.movilteleferico.back.model.rss;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Andres on 07/05/2016.
 */
@Root(name="content", strict = false)
public class Content {
    @Attribute(name="url")
    private String url;

    public Content() {
    }

    public Content(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

}
