package bo.miteleferico.movilteleferico.front.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import bo.miteleferico.movilteleferico.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements LocationListener {

    public final static int LINEAS_ACTUALES = 0;
    public final static int LINEAS_NUEVAS = 1;
    public final static int LINEAS_RIM = 2;

    private int opcionSeleccionada = 0;

    //Coordenas Linea Roja
    private LatLng r1 = new LatLng(-16.497386, -68.164521);
    private LatLng r2 = new LatLng(-16.498150, -68.152795);
    private LatLng r3 = new LatLng(-16.491441, -68.144682);

    //Coordenas Linea Amarilla
    private LatLng a1 = new LatLng(-16.518368, -68.150130);
    private LatLng a2 = new LatLng(-16.515308, -68.144022);
    private LatLng a3 = new LatLng(-16.514622, -68.130196);
    private LatLng a4 = new LatLng(-16.519468, -68.115551);

    //Coordenas Linea Verde
    private LatLng v1 = new LatLng(-16.519468, -68.115551);
    private LatLng v2 = new LatLng(-16.521455, -68.110397);
    private LatLng v3 = new LatLng(-16.526905, -68.100619);
    private LatLng v4 = new LatLng(-16.538255, -68.087103);

    // Coordenadas Linea Azul
    private LatLng az1 = new LatLng(-16.489838, -68.210135);
    private LatLng az2 = new LatLng(-16.492251, -68.183107);
    private LatLng az3 = new LatLng(-16.494801, -68.173625);
    private LatLng az4 = new LatLng(-16.497386, -68.164521);

    private SupportMapFragment mapFragment;
    private GoogleMap miMapa;


    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewContainer = inflater.inflate(R.layout.fragment_map, container, false);
       /* mapFragment = (SupportMapFragment) viewContainer.findFragmentById(R.id.mapMain);
        mapFragment.getMapAsync(this);*/

        mapFragment = new SupportMapFragment() {
            @Override
            public void onActivityCreated(Bundle savedInstanceState) {
                super.onActivityCreated(savedInstanceState);
                miMapa = mapFragment.getMap();
                if (miMapa != null) {
                    configurarMapa();
                    dibujarMapa(opcionSeleccionada);
                }
            }
        };
        getChildFragmentManager().beginTransaction().add(R.id.mapMain, mapFragment).commit();

        return viewContainer;
    }

    public void configurarMapa() {
        LatLng lapaz = new LatLng(-16.490102, -68.144710);
        miMapa.addMarker(new MarkerOptions().position(lapaz).title("La Paz"));
        miMapa.moveCamera(CameraUpdateFactory.newLatLngZoom(lapaz, 12));
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getActivity(), "Es necesario dar permisos de localizacion", Toast.LENGTH_LONG).show();
            return;
        }
        miMapa.setMyLocationEnabled(true);

    }

    public void dibujarMapa(int opcion) {
        opcionSeleccionada = opcion;
        if(miMapa != null) {
            miMapa.clear();
            switch (opcion) {
                case LINEAS_ACTUALES:
                    dibujarLineas();
                    break;
                case LINEAS_NUEVAS:
                    dibujarNuevasLineas();
                    break;
                case LINEAS_RIM:
                    dibujarLineas();
                    dibujarNuevasLineas();
                    break;
                default:
            }
        }

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private boolean dibujarLineas() {
        MarkerOptions rojo1 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(r1);
        MarkerOptions rojo2 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(r2);
        MarkerOptions rojo3 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(r3);

        MarkerOptions amarillo1 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(a1);
        MarkerOptions amarillo2 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(a2);
        MarkerOptions amarillo3 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(a3);
        MarkerOptions amarillo4 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(a4);

        MarkerOptions verde1 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(v1);
        MarkerOptions verde2 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(v2);
        MarkerOptions verde3 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(v3);
        MarkerOptions verde4 = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.mkr)).position(v4);

        miMapa.addMarker(rojo1);
        miMapa.addMarker(rojo2);
        miMapa.addMarker(rojo3);

        miMapa.addMarker(amarillo1);
        miMapa.addMarker(amarillo2);
        miMapa.addMarker(amarillo3);
        miMapa.addMarker(amarillo4);

        miMapa.addMarker(verde1);
        miMapa.addMarker(verde2);
        miMapa.addMarker(verde3);
        miMapa.addMarker(verde4);


        PolylineOptions lineaRoja = new PolylineOptions();
        lineaRoja.add(r1);
        lineaRoja.add(r2);
        lineaRoja.add(r3);
        lineaRoja.color(Color.RED);
        miMapa.addPolyline(lineaRoja);

        PolylineOptions lineaAmarilla = new PolylineOptions();
        lineaAmarilla.add(a1);
        lineaAmarilla.add(a2);
        lineaAmarilla.add(a3);
        lineaAmarilla.add(a4);
        lineaAmarilla.color(Color.YELLOW);
        miMapa.addPolyline(lineaAmarilla);

        PolylineOptions lineaVerde = new PolylineOptions();
        lineaVerde.add(v1);
        lineaVerde.add(v2);
        lineaVerde.add(v3);
        lineaVerde.add(v4);
        lineaVerde.color(Color.GREEN);
        miMapa.addPolyline(lineaVerde);
        return true;
    }

    private boolean dibujarNuevasLineas() {
        MarkerOptions azul1 = new MarkerOptions().position(az1);
        MarkerOptions azul2 = new MarkerOptions().position(az2);
        MarkerOptions azul3 = new MarkerOptions().position(az3);
        MarkerOptions azul4 = new MarkerOptions().position(az4);

        miMapa.addMarker(azul1);
        miMapa.addMarker(azul2);
        miMapa.addMarker(azul3);
        miMapa.addMarker(azul4);


        PolylineOptions lineaAzul = new PolylineOptions();
        lineaAzul.add(az1);
        lineaAzul.add(az2);
        lineaAzul.add(az3);
        lineaAzul.add(az4);
        lineaAzul.color(Color.BLUE);
        miMapa.addPolyline(lineaAzul);

        CircleOptions co = new CircleOptions();
        co.center(az1);
        co.fillColor(Color.WHITE);
        co.strokeColor(Color.BLACK);
        miMapa.addCircle(co);
        return true;
    }

}
