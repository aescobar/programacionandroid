package bo.miteleferico.movilteleferico.front.activitys;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import bo.miteleferico.movilteleferico.R;
import bo.miteleferico.movilteleferico.back.common.Parametros;
import bo.miteleferico.movilteleferico.back.services.RegistroAppService;
import bo.miteleferico.movilteleferico.front.fragments.InformacionContainerFragment;
import bo.miteleferico.movilteleferico.front.fragments.MapFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private MapFragment mapFragment;

    private BroadcastReceiver receiverRegistro;
    //TextView txvMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //push


       // txvMensaje = (TextView) findViewById(R.id.txvMensaje);

        receiverRegistro = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                String token = sp.getString(Parametros.TOKEN_REGISTRO, null);
                if (token != null) {
                   // txvMensaje.setText("El token es" + token);
                    Log.i("lll", "token: " + token);
                } else {
                    //txvMensaje.setText("El token no pudo ser recuperado");
                    Log.i("lll", "no hay token");
                }
            }
        };
        if (verificarPlayServices()) {
            Intent registroIntent = new Intent(MainActivity.this, RegistroAppService.class);
            startService(registroIntent);
        } else {
            //txvMensaje.setText("El dispositivo no es compatible");
        }

        iniciarFragmen();
    }

    private void iniciarFragmen() {
        Fragment fragment = new InformacionContainerFragment();
        if(fragment != null){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frmPrincipal, fragment).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverRegistro, new IntentFilter(Parametros.RECEIVER_REGISTRO_COMPLETO));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiverRegistro);
    }

    private boolean verificarPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultado = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultado == ConnectionResult.SUCCESS) {
            return true;
        } else {
            return false;
        }
    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if(id < 0)
            id = R.id.nav_informacion;
        Fragment fragment = null;
        if (id == R.id.nav_informacion) {
            fragment = new InformacionContainerFragment();
        } else if (id == R.id.nav_galeria) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_lineas_actuales) {
            fragment = crearMapFragment(MapFragment.LINEAS_ACTUALES);
        } else if (id == R.id.nav_lineas_nuevas) {
            fragment = crearMapFragment(MapFragment.LINEAS_NUEVAS);
        } else if (id == R.id.nav_lineas_rim) {
            fragment = crearMapFragment(MapFragment.LINEAS_RIM);
        }


        if(fragment != null){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frmPrincipal, fragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Fragment crearMapFragment(int opcion){
        if(mapFragment == null)
            mapFragment = new MapFragment();
        mapFragment.dibujarMapa(opcion);
        return  mapFragment;
    }

}
