package bo.miteleferico.movilteleferico.back.services;


import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by ichacolla on 05/05/2016.
 */
public class ActualizarTokenService extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        Intent intent =  new Intent(this,RegistroAppService.class);
        startService(intent);
    }
}
