package com.and104.cognos.lab20;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private EditText etxUrl;
    private ImageView imgImagen;
    private Button btnDescargar;
    private ProgressDialog cargando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etxUrl = (EditText) findViewById(R.id.etxUrl);
        btnDescargar = (Button) findViewById(R.id.btnCargar);
        imgImagen = (ImageView) findViewById(R.id.imgImagen);
        btnDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llamarTarea(etxUrl.getText().toString());
            }
        });


    }

    private void llamarTarea(String url){
        MiTareaAsincrona miTareaAsincrona = new MiTareaAsincrona(MainActivity.this);
        miTareaAsincrona.execute(url);

    }


    private class MiTareaAsincrona extends AsyncTask<String, Integer, Bitmap>{

        Context context;

        public MiTareaAsincrona(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            cargando = ProgressDialog.show(context,"Por favor espere....", "Descargando imagen");
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap imagenBitmap = null;
            try {
                imagenBitmap = Util.descargarImagen(params[0]);
            } catch (IOException e) {
                Log.e("MiTareaAsincrona", "Error al cargar imagen" + e.getMessage());
            }
            return imagenBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if(bitmap != null) {
                imgImagen.setImageBitmap(bitmap);
            }
            cargando.dismiss();

        }
    }


}
