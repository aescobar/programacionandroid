package com.and104.cognos.lab20;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by aescobar on 19/4/2016.
 */
public class Util {

    public static Bitmap descargarImagen(String url) throws IOException {
        URL direccion;
        InputStream inputStream;
        Bitmap imagen;
        direccion = new URL(url);
        inputStream = direccion.openStream();
        imagen = BitmapFactory.decodeStream(inputStream);
        inputStream.close();
        return  imagen;
    }

}
