package com.and104.cognos.lab06;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnLlamar;
    Button btnUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLlamar = (Button) findViewById(R.id.btnLlamada);
        btnUrl = (Button) findViewById(R.id.btnUrl);
        btnLlamar.setOnClickListener(this);
        btnUrl.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int op = v.getId();
        Intent intent = null;
        switch (op) {
            case R.id.btnLlamada:
                intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:70617136"));


                Toast t = Toast.makeText(getApplicationContext(), "si ENTRA", Toast.LENGTH_SHORT);
                t.show();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                this.startActivity(intent);
                break;
            case R.id.btnUrl:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:0,0?z=4&q=restaurantes"));
                this.startActivity(intent);
                break;

        }
    }
}
