package com.and104.cognos.lab10.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.and104.cognos.lab10.R;

/**
 * Created by aescobar on 1/4/2016.
 */
public class CustomView extends View{

    private int squareColor, labelColor;
    private String squareText;
    private Paint squarePaint;


    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        squarePaint = new Paint();
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomView,0,0);
        try {
            squareText = a.getString(R.styleable.CustomView_squareLabel);
            squareColor = a.getInteger(R.styleable.CustomView_squareColor, 0);
            labelColor = a.getInteger(R.styleable.CustomView_labelColor, 0);
        }finally {
            a.recycle();
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {
            squarePaint.setStyle(Paint.Style.FILL);
            squarePaint.setAntiAlias(true);

            squarePaint.setColor(squareColor);
            canvas.drawRect(0, 0, this.getMeasuredWidth(), this.getMeasuredHeight(), squarePaint);
            squarePaint.setColor(labelColor);
            squarePaint.setTextAlign(Paint.Align.CENTER);
            squarePaint.setTextSize(50);
            canvas.drawText(squareText, this.getMeasuredWidth() / 2, this.getMeasuredHeight() / 2, squarePaint);

       // super.onDraw(canvas);

    }

    public String getSquareText() {
        return squareText;
    }

    public void setSquareText(String squareText) {
        this.squareText = squareText;
        invalidate();
        requestLayout();
    }

    public int getLabelColor() {
        return labelColor;
    }

    public void setLabelColor(int labelColor) {
        this.labelColor = labelColor;
        invalidate();
        requestLayout();
    }

    public int getSquareColor() {
        return squareColor;
    }

    public void setSquareColor(int squareColor) {
        this.squareColor = squareColor;
        invalidate();
        requestLayout();
    }
}
