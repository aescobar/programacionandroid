package com.and104.cognos.lab10;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnFrag1;
    Button btnFrag2;
    Button btnFrag3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnFrag1 = (Button) findViewById(R.id.btnFrag1);
        btnFrag2 = (Button) findViewById(R.id.btnFrag2);
        btnFrag3 = (Button) findViewById(R.id.btnFrag3);

        btnFrag1.setOnClickListener(this);
        btnFrag3.setOnClickListener(this);
        btnFrag2.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        switch (v.getId()) {
            case R.id.btnFrag1:
                fragment = new PrimerFragment();
                break;
            case R.id.btnFrag2:
                fragment = new SegundoFragment();
                break;
            case R.id.btnFrag3:
                fragment = new TercerFragment();
                break;
        }
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.frmPrincipal, fragment).commit();

    }
}
