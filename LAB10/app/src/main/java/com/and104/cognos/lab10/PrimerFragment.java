package com.and104.cognos.lab10;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.and104.cognos.lab10.custom.CustomView;


/**
 * A simple {@link Fragment} subclass.
 */
public class PrimerFragment extends Fragment {


    public PrimerFragment() {
        // Required empty public constructor
    }

    private CustomView customView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View contarinerView = inflater.inflate(R.layout.fragment_primer, container, false);
        customView = (CustomView) contarinerView.findViewById(R.id.viewFrag1);
        customView.setSquareColor(Color.BLUE);
        customView.setLabelColor(Color.GREEN);
        customView.setSquareText("CLICK");
        customView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customView.setSquareColor(Color.RED);
                customView.setLabelColor(Color.BLACK);
                customView.setSquareText("CLICK DE NUEVO");
            }
        });
        return contarinerView;
    }

}
