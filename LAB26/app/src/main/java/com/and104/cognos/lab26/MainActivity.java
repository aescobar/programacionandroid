package com.and104.cognos.lab26;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private TextView txvLatitud;
    private TextView txvLongitud;
    private TextView txvEstado;
    private TextView txvProvider1;
    private TextView txvProvider2;
    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txvLatitud = (TextView) findViewById(R.id.txvLatitud);
        txvLongitud = (TextView) findViewById(R.id.txvLatitud);
        txvEstado = (TextView) findViewById(R.id.txvEstado);
        txvProvider1 = (TextView) findViewById(R.id.txvProvider1);
        txvProvider2 = (TextView) findViewById(R.id.txvProvider2);
    }

    public void iniciarGPS() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gpsHabilitado = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean networkHabilitado = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        txvEstado.setText("GPS: " + gpsHabilitado + " NETWORK: " + networkHabilitado);
        System.out.println("GPS: " + gpsHabilitado + " NETWORK: " + networkHabilitado);
        if (networkHabilitado) {
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this, "Los permiso son necesarios", Toast.LENGTH_LONG).show();
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }else{
            Toast.makeText(MainActivity.this, "Es necesarion encender el GPS", Toast.LENGTH_LONG).show();
        }

        if (gpsHabilitado) {
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this, "Los permiso son necesarios", Toast.LENGTH_LONG).show();
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }else{
            Toast.makeText(MainActivity.this, "Es necesarion encender el GPS", Toast.LENGTH_LONG).show();
        }

    }

    public void obtenerClick(View view){
        iniciarGPS();

    }

    @Override
    public void onLocationChanged(Location location) {
           Double latitud = location.getLatitude();
            Double longitud = location.getAltitude();

        txvLatitud.setText("Latitud: " + latitud.toString());
        txvLongitud.setText("Longitud: " + longitud.toString());

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        System.out.println(provider);
        if(provider.toUpperCase().contains("GPS")){
            txvProvider1.setText("GPS: Habilitado");
        }
        if(provider.toUpperCase().contains("NET")){
            txvProvider2.setText("NET: Habilitado");
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        System.out.println(provider);
        if(provider.toUpperCase().contains("GPS")){
            txvProvider1.setText("GPS: DesHabilitado");
        }
        if(provider.toUpperCase().contains("NET")){
            txvProvider2.setText("NET: DesHabilitado");
        }
    }
}
