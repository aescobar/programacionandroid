package com.and104.cognos.lab14;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ejemploandroid.EjemploAndroid;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void calcularClick(View view) {
        EditText etxPeso = (EditText) findViewById(R.id.etxPeso);
        EditText etxAltura = (EditText) findViewById(R.id.etxAltura);
        float peso = Float.parseFloat(etxPeso.getText().toString());
        float altura = Float.parseFloat(etxAltura.getText().toString());
        EjemploAndroid ea = new EjemploAndroid();
        String resultado =  ea.calculaIMC(peso, altura);
        Toast.makeText(MainActivity.this, resultado, Toast.LENGTH_LONG).show();

    }
}
