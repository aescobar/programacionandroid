package com.and104.cognos.lab28;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by aescobar on 3/5/2016.
 */
public class VerificarInternet extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        boolean conectado = estaPrendido(context);
        if(conectado){
            Toast.makeText(context, "Internet Prendido", Toast.LENGTH_SHORT);
            Intent i = new Intent(context, MiServicio.class);
            context.startService(i);
        }else{
            Toast.makeText(context, "Internet Apagado", Toast.LENGTH_SHORT);
        }

    }

    public boolean estaPrendido(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

}
