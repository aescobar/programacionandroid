package com.and104.cognos.lab28;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by aescobar on 3/5/2016.
 */
public class MiServicio extends IntentService{

    Handler handler;


    public MiServicio() {
        super("MiServicio");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        for(int i = 0; i<3;i++ ){
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Log.e("TAG", "Error sleep" + e.getMessage());
            }
            final int iter = i;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MiServicio.this, "Iteracion: " + (iter + 1), Toast.LENGTH_SHORT).show();
                }
            });

        }

    }
}
