package com.and104.cognos.lab07;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.and104.cognos.lab07.obj.Constantes;
import com.and104.cognos.lab07.obj.Contacto;

public class DescripcionActivity extends AppCompatActivity implements View.OnClickListener{

    TextView txv1;
    TextView txv2;
    TextView txv3;
    Contacto contac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion);
        txv1 = (TextView) findViewById(R.id.txvNombre);
        txv2 = (TextView) findViewById(R.id.txvNumero);
        txv3 = (TextView) findViewById(R.id.txvWeb);

        Bundle extras = getIntent().getExtras();
        if(extras == null)
            return;
        contac = (Contacto) getIntent().getSerializableExtra(Constantes.OBJETO_CONTACTO);
        txv1.setText(contac.getNombre());
        txv2.setText(contac.getNumero());
        txv3.setText(contac.getWeb());
        txv2.setOnClickListener(this);
        txv3.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent();
        switch (v.getId()){
            case R.id.txvNumero:
                i.putExtra(Constantes.RESPUESTA, contac.getNumero());
                setResult(0, i);
                break;
            case R.id.txvWeb:
                i.putExtra(Constantes.RESPUESTA, contac.getWeb());
                setResult(1, i);
                break;
        }
        finish();
    }
}
