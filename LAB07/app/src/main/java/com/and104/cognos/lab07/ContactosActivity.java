package com.and104.cognos.lab07;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.and104.cognos.lab07.obj.Constantes;
import com.and104.cognos.lab07.obj.Contacto;

import java.util.ArrayList;

public class ContactosActivity extends AppCompatActivity {

    private final int NUMERO = 0;
    private final int WEB = 1;
    private ListView ltvwContactos;
    private ArrayAdapter<String> adapter;
    private ArrayList<Contacto> contactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactos);
        ltvwContactos = (ListView) findViewById(R.id.ltvwContactos);

        contactos = new ArrayList<>();
        Contacto c1 = new Contacto("Perssona 1","70617136","www.miteleferico.bo");
        contactos.add(c1);
        Contacto c2 = new Contacto("Persona 2","70617136","www.miteleferico.bo");
        contactos.add(c2);
        Contacto c3 = new Contacto("Persona 3","70617136","www.miteleferico.bo");
        contactos.add(c3);
        ArrayList<String> lstString = new ArrayList<>();
        for(Contacto item : contactos){
            lstString.add(item.getNombre());
        }

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, lstString);
        ltvwContactos.setAdapter(adapter);
        ltvwContactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), DescripcionActivity.class);
                i.putExtra(Constantes.OBJETO_CONTACTO, contactos.get(position));
                startActivityForResult(i, 0);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle result = data.getExtras();
        String resp = result.getString(Constantes.RESPUESTA);
        switch (resultCode) {
            case NUMERO:
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Toast toas = Toast.makeText(getApplicationContext(), "NO SE TIENE PERMISOS", Toast.LENGTH_SHORT);
                    toas.show();
                    return;
                }
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + resp)));
                break;
            case WEB:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + resp)));
                break;

        }

    }

}
