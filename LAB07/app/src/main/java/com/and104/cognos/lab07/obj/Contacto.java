package com.and104.cognos.lab07.obj;

import java.io.Serializable;

/**
 * Created by aescobar on 31/3/2016.
 */
public class Contacto implements Serializable {
    private String nombre;
    private String numero;
    private String web;

    public Contacto(String nombre, String numero, String web) {
        this.nombre = nombre;
        this.numero = numero;
        this.web = web;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }
}
